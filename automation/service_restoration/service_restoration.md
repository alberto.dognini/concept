# Service Restoration

## Overview
<img src="docs/service_restoration_fm.png" width="829" height="300">

The service restoration (SR) component, as middleware, has a standard interface for the data exchange and is independent on the specific distribution grid to be managed. In the FISMEP platform, the real-time data for the execution of SR (status of the switches, measurements of power injections, voltages or currents) are retrieved from the Orion Context Broker (CB). If the status of the switches indicate the presence of a fault (tripped circuit breakers), a notification condition is triggered: the CB sends a message to SR service, which starts to process the information and sends a message back to the CB to retrieve the data related to the network. The SR algorithm iteratively re-energizes each disconnected load, until all possible loads are reconnected or the safety electrical constraints are violated; eventually provides the solution to the CB. Finally, the CB issues the network reconfiguration commands to the switching devices in the grid that need to be closed. Once the closing operation is successfully performed, data are updated in the CB.

## Descripton
To manage the distribution grids in case of electrical fault, the service restoration algorithm is implemented as middleware component in the cloud-based FISMEP platform.

In the automated electrical grids, once the fault is located and isolated by the nearest switching devices, the SR algorithm operates the reconfiguration of the network in order to re-energize the disconnected customers.
Among the disconnected loads, the one with the highest priority (e.g. hospital, transportation, communication or gas network site) is chosen as target for the restoration.
Successively, the SR algorithm determines the most suitable primary substation to re-energize the target node through an alternative feeder, by closing the normally open tie switch.

Each network topology, obtained by closing the bus-tie unit for each substation in the network that can reconnect the target node, is to be checked versus security limits via state estimation (SE) algorithm. The SE method used in this implementation relies on the Weighted Least Squares (WLS) approach, which is based on the minimization of the square of the measurement residual vector.

For each possible alternative grid topology, the respect of the following constraints is verified:
* **Radiality of the network**: the restoration scheme must maintain each substation electrically disconnected from the others
* **Voltage limits**: at each node $`(x)`$, the estimated voltage $`\bar{V}_x`$ with a confidence interval of $`3\sigma`$ must remain within the range ±10% of the nominal value $`V_n`$<br>
$`0.9 \cdot V_n < |\bar{V}_x| ± 3\sigma_{V_x} < 1.1 \cdot V_n`$<br>
* **Loading limits**: : for each edge $`(x,y)`$, the estimated line current with a confidence interval of $`3\sigma`$ must remain below the ampacity $`I_{max}`$ of the line or of the overcurrent limit of the transformer in the primary substation<br>
$`|\bar{I}_{x,y}| ± 3\sigma_{I_{x,y}} < I_{max}`$.<br>

The SR algorithm determines the most optimal solution still using the outcomes of SE, by implementing a Multiple-Criteria Decision Analysis (MCDA) approach, which considers as objectives the simultaneous minimization of (i) total power losses and (ii) the utilization of electrical lines $`\theta_{x,y}`$. This latter aspect indicates the current that can still flow in a line in relation to its maximum value and is computed as:<br>
$`\theta_{x,y} = \frac{I_{max} - |\bar{I}_{x,y}|}{I_{max} } `$<br>

After the selected tie unit successfully closes, the process restarts and continues until all the de-energized loads are reconnected or the constraints are violated.

## Related Links

* [RBOSR](https://git.rwth-aachen.de/acs/public/automation/rbosr) - Rule-Based Optimized Service Restoration

## References

* M. Haghgoo, A. Dognini and A. Monti, "*A Cloud-Based Platform for Service Restoration in Active Distribution Grids*," 2020 6th IEEE International Energy Conference (ENERGYCon), Gammarth, Tunisia, 2020, pp. 841-846, doi: [10.1109/ENERGYCon48941.2020.9236560](https://doi.org/10.1109/ENERGYCon48941.2020.9236560).
* A. Dognini, A. Sadu, A. Angioni, F. Ponci and A. Monti, "*Service Restoration Algorithm for Distribution Grids under High Impact Low Probability Events*," 2020 IEEE PES Innovative Smart Grid Technologies Europe (ISGT-Europe), The Hague, Netherlands, 2020, pp. 237-241, doi: [10.1109/ISGT-Europe47291.2020.9248823](https://doi.org/10.1109/ISGT-Europe47291.2020.9248823).
* N. Wirtz, A. Dognini, A. Sadu, A. Monti, G. Brito, G. di Orio, P. Maló, C.S. Nechifor, "*Securing CEI by-design*," Cyber-Physical Threat Intelligence for Critical Infrastructures Security: A Guide to Integrated Cyber-Physical Protection of Modern Critical Infrastructures, now Publishers Inc. (2020), pp. 245-274, doi: [10.1561/9781680836875.ch14](http://dx.doi.org/10.1561/9781680836875.ch14)
* M. Haghgoo, A. Dognini, T. Storek, R. Plamanescu, U. Rahe, S. Gheorghe, M. Albu, A. Monti, D. Müller, "*Open Smart Energy Eco-System for the Future*," IOP Conference Series: Earth and Environmental Science, vol. 588, pp 1.01 – 1.05, doi: [10.1088/1755-1315/588/2/022048](http://dx.doi.org/10.1088/1755-1315/588/2/022048)
