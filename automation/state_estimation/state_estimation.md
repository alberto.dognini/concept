# State Estimatnio

## Overview
<img src="docs/state_estimation_fm.png" width="829" height="300">

The state estimation (SE) component, as middleware, has a standard interface for the data exchange. In the FISMEP platform, the real-time data for the execution of SE (measurements of power injections, voltages or currents) are retrieved from the Orion Context Broker (CB). The SE algorithm is queried by the SR for each possible alternative network configuration proposed.

## Descripton
The adopted state estimation method is based on the Weighted Least Squares (WLS) approach, which is based on the minimization of the square of the measurement residual vector $`\mathbf{e}_z = \mathbf{z} − h(\hat{\mathbf{x}})`$.
The measurement residual is the difference between the input measurements $`\mathbf{z}`$ and the measurement function vector $`h(\hat{\mathbf{x}})`$. The measurement function derives the measurement values based on the estimated states $`\hat{\mathbf{x}}`$. If the measurement function is linear, then $`h(\hat{\mathbf{x})} = \mathbf{H}\hat{\mathbf{x}}`$, where $`\mathbf{H}=\frac{∂h(\hat{\mathbf{x}})}{∂\hat{\mathbf{x}}}`$.
The square of the measurement residual is defined as $`J=\mathbf{e}_z {\mathbf{e}_z}^T`$. Additionally, each element of the measurement residual $`\mathbf{e}_z_k`$ is multiplied by a weight, that is defined as the inverse of the input measurement variance. J is therefore redefined as $`\mathbf{J}=\mathbf{e}_z \mathbf{R}^{-1} {\mathbf{e}_z}^T`$, where $`\mathbf{R}`$ is the measurement covariance matrix.
Eventually the expression of the state, when the measurement function is linear, is obtained by setting $`\frac{∂\mathbf{J}}{∂\hat{\mathbf{x}}}=0`$ in

$`\hat{\mathbf{x}}=(\mathbf{H}^T \mathbf{R}^{-1} \mathbf{H})^{-1}\mathbf{H}^T \mathbf{R}^{-1} \mathbf{z}`$.

In case the measurement function is non-linear the state estimates can be calculated via the *g* Newton-Raphson (NR) iteration defined as

$`\hat{\mathbf{x}}^g=\hat{\mathbf{x}}^{g-1}-(\mathbf{H}^T \mathbf{R}^{-1} \mathbf{H})^{-1}\mathbf{H}^T \mathbf{R}^{-1} {\mathbf{e}_z}^{g-1}`$.

In distribution system, the WLS-SE is adapted into the Distribution System State Estimator (DSSE) which exploits power system equation and methods for system linearization peculiar for distribution networks. The measurements considered are power injection or generation at all nodes except at the slack bus, for which voltage magnitude is adopted. The relative standard deviation of the voltage magnitude measurement is 1%, whereas the relative standard deviation of the active and reactive power measurements is √2%.
The estimated state is the real and imaginary part of voltage phasor at each node. The real and imaginary current are derived by the voltages and the model of the network.
The measurement function element corresponding to the slack bus voltage magnitude $`|V_1|`$ measurement is obtained by converting the measurement to real and imaginary part

$`h(\hat{\mathbf{x}})_{[1]}=|V_1|cos(0)=|V_1|`$

$`h(\hat{\mathbf{x}})_{[2]}=|V_1|sin(0)=0`$.

The measurement function elements corresponding to the power injection measurements ($`P_i,Q_i`$) at node $`i`$ are obtained by linearizing the power to an injection current. At each *g* NR iteration the measurement function is calculated as

$`h(\hat{\mathbf{x}})_{[2i+2]}=\Re\left(\frac{P_i+jQ_i}{V_i^{g-1}} \right)`$

$`h(\hat{\mathbf{x}})_{[2i+3]}=\Im\left(\frac{P_i+jQ_i}{V_i^{g-1}} \right)`$,

where $`V_i^{g-1}`$ is the voltage phasor estimated at node *i* in the previous NR iteration.

## Related Links

* [RBOSR](https://git.rwth-aachen.de/acs/public/automation/rbosr) - Rule-Based Optimized Service Restoration

## References

* R. Stengel, Optimal Control and Estimation, ser. Dover Books on Mathematics. Dover Publications, 2012. [Online](https://books.google.de/books?id=JqDDAgAAQBAJ).
* C. Muscas, S. Sulis, A. Angioni, F. Ponci and A. Monti, "Impact of Different Uncertainty Sources on a Three-Phase State Estimator for Distribution Networks," in IEEE Transactions on Instrumentation and Measurement, vol. 63, no. 9, pp. 2200-2209, Sept. 2014, doi: [10.1109/TIM.2014.2308352](https://doi.org/10.1109/TIM.2014.2308352).
* A. Angioni et al., "Design and Implementation of a Substation Automation Unit," in IEEE Transactions on Power Delivery, vol. 32, no. 2, pp. 1133-1142, April 2017, doi: [10.1109/TPWRD.2016.2614493](https://doi.org/10.1109/TPWRD.2016.2614493).
