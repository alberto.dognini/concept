# Automation layer

<img src="docs/fismep_platform.png"  width="917" height="300">

## Description
The automation layer is based on the [FIWARE](https://www.fiware.org/developers) platform, in which a custom selection of generic enablers allow the implementation of specific functions. 

## Features
* **Context Broker Orion**: key service that provides update, query, registration and subscription functionality via API; it allows to manage, process and exploit context information, as well as data streams, in real-time, at massive scale and in distributed form; it uses MongoDB as an underling database for storing data
* **Quantum Leap**: service created specifically for storing and managing time series data; a given content is subscribed and automatically stored in the connected high performance CrateDB database
* **IoT Agent**: component that lets a group of devices send their data to and be managed from a Context Broker, translating their own native protocols into NGSI format
* **MQTT broker**: a server that receives all messages from the clients and then routes the messages to the appropriate destination clients
* **IoT Device**: each device in lower distribution/application layer periodically report power/voltage/current readings and the status of the internal sensors; additionally, each device can receive remote commands

## Services
* [Grid Monitoring](): visualize the current status of the grid
* [State Estimation](state_estimation/state_estimation.md): estimates the electrical state of a network by eliminating inaccuracies and errors from measurement data
* [Service Restoration](service_restoration/service_restoration.md): performs a FLISR procedure (Fault Location, Isolation, and Service Restoration)
