# ![LEGOS](docs/legos_logo.png) <br/> Lite Emulator of Grid Operations
## Description
The Lite Emulator of Grid Operations (LEGOS) is an educational multi-layer platform for demonstrating smart energy service use cases in a real down-scaled power grid. Composed of modular interlocking units, the network topology can be reshaped to match the desired layout, allowing the power to flow between connected entities, while using led strips activation patterns to visualize flow magnitude and direction. Each entity implements common energy-related behavioral patterns, which can be influenced by direct haptic interaction or, as  IoT device, by remote control services. Data from all IoT devices are collected, processed and stored in a cloud platform to provide smart automation services.

<img src="docs/overview.png"  width="1200" height="516">

## Smart Grid architecture
The architecture of LEGOS consists of three stacked layers: a lower mechanical/electrical layer composed by branches and nodes, a grid that distributes the power among the entities that reside on the middle application layer. The coordination of the demonstrator relies on a single-board computer which provides edge-cloud services in the upper automation layer.

[**Distribution layer**](distribution/distribution_layer.md): using the building blocks of the lower layer, any grid topology can be implemented in the form of a triangular mesh, where each node allows a maximum of six branches to be connected. If nodes are passive units for the branch interconnection, branches are rather smart units that actively control and visualize the power flowing across them, by measuring and logging telemetry data via Wi-Fi to the edge-cloud service, generating led sequence activation proportional to the measured current, emulating grid faults in the form of open-circuit or short-circuit to ground, and enabling/disabling the current flow using integrated switches that provide network reconfiguration capabilities. Branches are fully controlled remotely, but a direct interaction is possible through specific smart placeholder that can be placed on the branch to induce a fault.

[**Application layer**](application/application_layer.md): different entities can be easily placed on the nodes using custom designed spring-loaded connectors. Their behavior reflects actual entities on a small scale, whose activity results in current being sinked or sourced to the grid, according to their energy profile as producer/consumer/prosumer. Every entity is able to measure the power absorbed or generated and to log data via Wi-Fi to the edge-cloud service. Additionally, each entity offers several ways of direct interaction via capacitive touch buttons, but can also be fully controlled via FIWARE or through direct web access using a smartphone when direct contact is not possible.

[**Automation layer**](automation/automation_layer.md): the edge-cloud services are offered via FIWARE, a framework of open-source platform components, developed to accelerate the development of smart internet-related applications within various domains [[1](http://fiware.github.io/specifications/ngsiv2/stable)]. Besides being based on a distributed architecture, FIWARE relies on a large community and on a set of components accessible via standard Application Program-ming Interface (API) so called Next Generation Service Interface (NGSI), which is currently a de-facto standard released by the Open Mobile Alliance (OMA) [[2](https://omaspecworks.org/)] and used in the context information management system of smart energy domain. FIWARE manages the entire lifecycle of context information via Orion Context Broker, stores data via mongoDB and communicates with the devices via IoT Agent. 


### LEGOS in Numbers
* 4897 Electronic components (1297 hand + 3600 automatic assembly)
* 355  Transducers (243 sensors + 112 actuators)
* 53   Power measurements units (3 different accuracy classes)
* 32   IoT devices (12 unique + 20 same identity profile) 
* 15   Electronics circuits designed and manufactured
* 12   Months team work
* 8    People (1 PostDoc, 3 PhD students, 4 students)
* 2    Institutes (ACS and EBC)

## Copyright

2021, Institute for Automation of Complex Power Systems, EONERC

## License

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.

## Funding
<a rel="funding" href="https://fismep.de/"><img alt="FISMEP" style="border-width:0" src="docs/fismep_logo.png" width="78" height="63"/></a><br />This work was supported by <a rel="fismep" href="https://fismep.de/">FIWARE for Smart Energy Platform</a> (FISMEP), a German project funded by the *Ministry for Industry and Energy* and the *ERA-Net Smart Energy Systems* Programme under Grant 0350018A.

## Developers
- **Carlo Guarnieri**       (Concept and design coordination)
- **Alberto Dognini**       (Services for automation layer)
- **Maliheh Haghgoo**       (Automation platform architecture)
- **Thomas Storek**         (Automation platform configuration)
- **Daniel Wienands**       (Software, hardware and 3D design)
- **Simon Melcher**         (Software, hardware and 3D design)
- **Dominik Moll**          (Software, hardware and 3D design)
- **Lukas Jeske**           (Software, hardware and 3D design)

## Contact

[![EONERC ACS Logo](docs/eonerc_logo.png)](http://www.acs.eonerc.rwth-aachen.de)

- [Dr. Carlo Guarnieri Calò Carducci](mailto:cguarnieri@eonerc.rwth-aachen.de)

[Institute for Automation of Complex Power Systems (ACS)](http://www.acs.eonerc.rwth-aachen.de)  
[EON Energy Research Center (EONERC)](http://www.eonerc.rwth-aachen.de)  
[RWTH University Aachen, Germany](http://www.rwth-aachen.de)  

