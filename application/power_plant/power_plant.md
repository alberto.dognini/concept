# Power Plant

## Overview
<img src="docs/power_plant_fm.png"  width="386" height="300">

[<img src="docs/power_plant_3d_1.png"  width="87" height="100">](docs/power_plant_3d_1.png)
&nbsp;
[<img src="docs/power_plant_3d_2.png"  width="90" height="100">](docs/power_plant_3d_2.png)


### Descripton
The power plant is the largest power producer running on traditional fossil fuel and providing up to the 60% of the total power flowing in the grid. The higher is the electricity demand in the grid, the higher is the load for the power plant, resulting in an increase in the alternator rotation speed and in a transition of the tower color from green to yellow and eventually to red at maximum load.

### Interaction
<img src="docs/power_plant_bd.png"  width="507" height= "150">

### Features
<img src="docs/power_plant_ft_1.png"  width="520" height= "300">
<img src="docs/power_plant_ft_2.png"  width="249" height= "300">
<img src="docs/power_plant_ft_3.png"  width="303" height= "300">

## Technical Details

|                   |                               |
| ----              | ----                          |
| Dimensions        | 140 x 160 x 180 [mm] (WxLxH)  |
| Operating voltage | 5 [V]                         |
| Monitored bus     | 3.3 [V]                       |
| Shunt resistance  | 20 [mΩ]                       |
| Start delay       | 5 [ms]                        |

## Power consumption
The budget is based on nominal values from datasheet specifications; the typical value for led is referred to a 50% dutycycle.

### **3.3V bus**
| Part      | Count     | Typical | Max     |
| --------- | :-------: | ------: | ------: |
| ESP32     | 1         | 160 mA  |  260 mA |
| INA 223	| 2			|		  |   10 mA |
| Total     |           |         |  270 mA |


### **5V bus**
| Part      | Count     | Typical | Max     |
| --------- | :-------: | ------: | ------: |
| LT3081    |  2        |         | 3800 mA |
| LED Red   |  6        |  30 mA  |   60 mA |
| LED Green |  6        |  30 mA  |   60 mA |
| LED Yellow|  6        |  60 mA  |  120 mA |
| Fan       |  1        |         |         |
| Motor     |  1        |         |   30 mA |
| Total     |           |         | 4070 mA |

## Media

[<img src="docs/power_plant_1.png" width="100" height= "100">](docs/power_plant_1.png)
&nbsp;
[<img src="docs/power_plant_2.png" width=" 69" height= "100">](docs/power_plant_2.png)

## Related Links

* [Hardware]()
* [Firmware]() 
* [Assembly]()