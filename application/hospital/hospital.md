# Hospital

## Overview
<img src="docs/hospital_fm.png"  width="329" height="300">

[<img src="docs/hospital_3d_1.png"  width="111" height="100">](docs/hospital_3d_1.png)
&nbsp;
[<img src="docs/hospital_3d_2.png"  width="101" height="100">](docs/hospital_3d_2.png)

### Descripton
The hospital is an essential health care infrastructure, which provides indispensable and critical services for the society. Being crucial for the continuity of medical equipment, even during a possible power grid failure event, the hospital operation is supported by an internal uninterruptible power supply (UPS) that allows to continue operating during a fault. During normal operation, the UPS uses the energy from grid to load the internal battery storage, whereas during a fault the UPS provides the required power. The power load of the hospital on the grid depends on the medical devices in use, which can be influenced by activating/disactivating the emergency status of two rooms: the ground floor room is activated by the presence of an ambulance in front of the entrance of the building; the second floor room is activated by the presence of an helicopter on the roof.

### Interaction
<img src="docs/hospital_bd.png"  width="539" height= "275">

### Features
<img src="docs/hospital_ft_1.png"  width="511" height= "300">
<img src="docs/hospital_ft_2.png"  width="503" height= "300">

## Technical Details

|                   |                               |
| ----              | ----                          |
| Dimensions        | 109 x 106 x 83 [mm] (WxLxH)   |
| Operating voltage | 3.3 [V]                       |
| Monitored bus     | 3.3 [V]                       |
| Shunt resistance  | 100 [mΩ]                      |
| Start delay       | 500 [ms]                      |

## Power consumption
The budget is based on nominal values from datasheet specifications; the typical value for led is referred to a 50% dutycycle.

### **3.3V bus**
| Part      | Count     | Typical | Max     |
| --------- | :-------: | ------: | ------: |
| ESP32     | 1         | 160 mA  |  260 mA |
| LED Lights| 6 		|  30 mA  |  60 mA	|
| LED Helicopter  | 4	|  20 mA  |  40 mA	|
| LED Ambulance   | 4	|  20 mA  |  40 mA	|
| UPS       | 1 		|  		  |  100 mA |
| INA 223	| 1			|		  |    5 mA	|
| Total     |           |         |  505 mA |


### **5V bus**
| Part      | Count     | Typical | Max     |
| --------- | :-------: | ------: | ------: |
| LED UPS   | 4 		| 40 mA   | 80 mA	|
| Total     |           |         | 80 mA   |

## Media

[<img src="docs/hospital_1.png" width="124" height= "100">](docs/hospital_1.png)
&nbsp;
[<img src="docs/hospital_2.png" width="101" height= "100">](docs/hospital_2.png)

## Related Links

* [Hardware]()
* [Firmware]() 
* [Assembly]()