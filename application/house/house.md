# House

## Overview
<img src="docs/house_fm.png"  width="607" height="300">

[<img src="docs/house_3d_1.png"  width="106" height="100">](docs/house_3d_1.png)
&nbsp;
[<img src="docs/house_3d_2.png"  width="113" height="100">](docs/house_3d_2.png)
&nbsp;
[<img src="docs/house_3d_3.png"  width="104" height="100">](docs/house_3d_3.png)
&nbsp;
[<img src="docs/house_3d_4.png"  width="096" height="100">](docs/house_3d_4.png)


### Descripton
The house represents a common residential prosumer, which requires energy from the grid for operating the household appliances, but can also rely on the energy from PV cells installed on the roof. When the power generated exceeds the power requested by the building, the surplus power flows back into the grid. The electrical load is mainly constituted by the thermoregulation appliances: once the desired temperature inside the building is set, a PID controller drives a thermo-electric module (TEM) to either heat or cool the environment, until the target temperature is reached.



### Interaction
<img src="docs/house_bd.png"  width="333" height= "275">

### Features
<img src="docs/house_ft_1.png"  width="676" height= "300">
<img src="docs/house_ft_2.png"  width="603" height= "300">
<img src="docs/house_ft_3.png"  width="298" height= "300">

## Technical Details

|                   |                               |
| ----              | ----                          |
| Dimensions        | 108 x 120 x 100 [mm] (WxLxH)  |
| Operating voltage | 3.3 [V]                       |
| Monitored bus     | 3.3 [V]                       |
| Shunt resistance  | 2 x 100 [mΩ]                  |
| Start delay       | 500 [ms]                      |

## Power consumption
The budget is based on nominal values from datasheet specifications; the typical value for led is referred to a 50% dutycycle.

### **3.3V bus**
| Part      | Count     | Typical | Max     |
| --------- | :-------: | ------: | ------: |
| ESP32     | 1         | 160 mA  |  260 mA |
| TEM       | 1         |         |  500 mA |
| Fan       | 2         | 144 mA  |  166 mA |
| LED App   | 4         |  50 mA  |  110 mA |
| INA 223	| 2			|		  |   10 mA |
| Total     |           |         | 1046 mA |


### **5V bus**
| Part      | Count     | Typical | Max     |
| --------- | :-------: | ------: | ------: |
| LT3081    |  1        |         |  200 mA |
| LED Solar |  16       |  80 mA  | 160 mA  |
| LED Grid  |   8       |  40 mA  |  80 mA  |
| LED Dislay|   3 x 8   |  90 mA  | 180 mA  |
| Total     |           |         | 620 mA  |

## Media

[<img src="docs/house_1.png" width="114" height= "100">](docs/house_1.png)
&nbsp;
[<img src="docs/house_2.png" width="117" height= "100">](docs/house_2.png)
&nbsp;
[<img src="docs/house_3.png" width="118" height= "100">](docs/house_3.png)
&nbsp;
[<img src="docs/house_4.png" width="149" height= "100">](docs/house_4.png)

## Related Links

* [Hardware]()
* [Firmware]() 
* [Assembly]()