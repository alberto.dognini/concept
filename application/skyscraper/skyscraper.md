# Skyscraper

## Overview
<img src="docs/skyscraper_fm.png"  width="464" height="300">

[<img src="docs/skyscraper_3d_1.png"  width="69" height="100">](docs/skyscraper_3d_1.png)


### Descripton
The skyscraper is typical aggregation of residential spaces and commercial activities.Integrates a HVAC system for improving the occupants comfort by regulating the indoor temperature, monitoring the indoor air quality and recirculating the air. The first and third floors host respectively a cyber-security center that monitors possible threats on critical infrastructure, like cyber-attacks to industrial control systems (ICS) or data-breaches, and a SCADA center that constantly monitors the status of the grid: power generation, consumption and eventual faults. The second floor is a private apartment, the TV activity is connected to the eventual sport activities in the stadium.

### Interaction
<img src="docs/skyscraper_bd.png"  width="294" height= "350">

### Features
<img src="docs/skyscraper_ft_1.png"  width="558" height= "300">
<img src="docs/skyscraper_ft_2.png"  width="627" height= "300">
<img src="docs/skyscraper_ft_3.png"  width="493" height= "300">
<img src="docs/skyscraper_ft_4.png"  width="475" height= "300">

## Technical Details

|                   |                               |
| ----              | ----                          |
| Dimensions        | 110 x 110 x 197 [mm] (WxLxH)  |
| Operating voltage | 3.3 [V]                       |
| Monitored bus     | 3.3 [V]                       |
| Shunt resistance  | 100 [mΩ]                      |
| Start delay       | 500 [ms]                      |

## Power consumption
The budget is based on nominal values from datasheet specifications; the typical value for led is referred to a 50% dutycycle.

### **3.3V bus**
| Part      | Count     | Typical | Max     |
| --------- | :-------: | ------: | ------: |
| ESP32     |  1        | 160 mA  |  260 mA |
| Fan       |  1        |         |   83 mA |
| INA 223	|  1    	|		  |    5 mA |
| IAQ       |  1        |         |   33 mA |
| Servo     |  1 		|         |  		|
| Heating   |  1        |         |  110 mA |
| LED SCADA |  5        |  25 mA  |   50 mA |
| LED EXT   | 12        | 120 mA  |  240 mA |
| LED INT   |  3        |  30 mA  |   60 mA |
| LED TV    |  1        |  10 mA  |   20 mA |
| Total     |           |         |  861 mA |

## Media

[<img src="docs/skyscraper_1.png" width="51" height= "100">](docs/skyscraper_1.png)
&nbsp;
[<img src="docs/skyscraper_2.png" width="60" height= "100">](docs/skyscraper_2.png)
&nbsp;
[<img src="docs/skyscraper_3.png" width="54" height= "100">](docs/skyscraper_3.png)

## Related Links

* [Hardware]()
* [Firmware]() 
* [Assembly]()