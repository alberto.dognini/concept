# Supermarket

## Overview
<img src="docs/supermarket_fm.png"  width="337" height="300">

[<img src="docs/supermarket_3d_1.png"  width=" 94" height="100">](docs/supermarket_3d_1.png)
&nbsp;
[<img src="docs/supermarket_3d_2.png"  width="121" height="100">](docs/supermarket_3d_2.png)


### Descripton
The supermarket represents a common commercial prosumer, which requires energy from the grid for operating the commercial appliances, but can also rely on the energy from gas combustion using a combined heat and power (CHP) system. The mixing percentage of the two energy sources is decided on the basis of the respective market cost in €/kWh. When the power generated using gas exceeds the power requested by the building, the surplus power flows back into the grid. The electrical load is mainly constituted by the electrical energy required for running the commercial appliances and by the energy required for thermoregulation, which is converted with different efficiency from electricity (45%) and from gas (80%) and stored in a dedicated thermal storage element, waiting to be used upon need.

### Interaction
<img src="docs/supermarket_bd.png"  width="333" height= "275">

### Features
<img src="docs/supermarket_ft_1.png"  width="595" height= "300">
<img src="docs/supermarket_ft_2.png"  width="572" height= "300">

## Technical Details

|                   |                               |
| ----              | ----                          |
| Dimensions        | 106 x 98 x 83 [mm] (WxLxH)    |
| Operating voltage | 3.3 [V]                       |
| Monitored bus     | 3.3 [V]                       |
| Shunt resistance  | 2 x 100 [mΩ]                  |
| Start delay       | 500 [ms]                      |

## Power consumption
The budget is based on nominal values from datasheet specifications; the typical value for led is referred to a 50% dutycycle.

### **3.3V bus**
| Part      | Count     | Typical | Max     |
| --------- | :-------: | ------: | ------: |
| ESP32     | 1         | 160 mA  |  260 mA |
| LED App   | 12        |  60 mA  |  120 mA |
| INA 223	| 2			|		  |   10 mA |
| Total     |           |         | 1046 mA |


### **5V bus**
| Part      | Count     | Typical | Max     |
| --------- | :-------: | ------: | ------: |
| LT3081    |  1        |         |  200 mA |
| LED Gas   |   8       |  40 mA  |  80 mA  |
| LED Grid  |   7       |  35 mA  |  70 mA  |
| LED Dislay|   3 x 8   |  90 mA  | 180 mA  |
| Total     |           |         | 620 mA  |

## Media

[<img src="docs/supermarket_1.png" width=" 91" height= "100">](docs/supermarket_1.png)
&nbsp;
[<img src="docs/supermarket_2.png" width="139" height= "100">](docs/supermarket_2.png)
&nbsp;
[<img src="docs/supermarket_3.png" width="144" height= "100">](docs/supermarket_3.png)
&nbsp;
[<img src="docs/supermarket_4.png" width=" 94" height= "100">](docs/supermarket_4.png)

## Related Links

* [Hardware]()
* [Firmware]() 
* [Assembly]()