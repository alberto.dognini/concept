# Wind Farm 

## Overview
<img src="docs/wind_farm_fm.png"  width="441" height="300">

[<img src="docs/wind_farm_3d_1.png"  width="73" height="100">](docs/wind_farm_3d_1.png)


### Descripton
The wind farm is one of the power producers running on renewable energy, in the specific by converting the wind energy into electricity. The higher is the intensity of the wind flow, settable via touch slider, the higher is the power produced and injected into the grid. The current power production is shown by a LED indicator, where the maximum represents a contribution of 20% to the total production.

### Interaction
<img src="docs/wind_farm_bd.png"  width="776" height= "280">

### Features
<img src="docs/wind_farm_ft_1.png"  width="362" height= "300">

## Technical Details

|                   |                               |
| ----              | ----                          |
| Dimensions        | 106 x 106 x 217 [mm] (WxLxH)  |
| Operating voltage | 5 [V]                         |
| Monitored bus     | 3.3 [V]                       |
| Shunt resistance  | 75 [mΩ]                       |
| Start delay       | 50 [ms]                       |

## Power consumption
The budget is based on nominal values from datasheet specifications; the typical value for led is referred to a 50% dutycycle.

### **5V bus**
| Part      | Count     | Typical | Max     |
| --------- | :-------: | ------: | ------: |
| LT3081    | 1         |         | 1000 mA |
| ESP32     | 1         | 160 mA  |  260 mA |
| Turbine   | 1		    |         |   80 mA	|
| LED Gauge | 1(5) 		| 10 mA   |  100 mA	|
| Total     |           |         | 1610 mA |


## Media

[<img src="docs/wind_farm_1.png" width="64" height= "100">](docs/wind_farm_1.png)
&nbsp;

## Related Links

* [Hardware]()
* [Firmware]() 
* [Assembly]()