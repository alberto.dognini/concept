# Substation

## Overview
<img src="docs/substation_fm.png"  width="324" height="300">

[<img src="docs/substation_3d_1.png"  width="108" height="100">](docs/substation_3d_1.png)

### Descripton
The substation is an essential element for the automation of an electrical grid, which provides monitoring and protection functions. In case of fault, the instruments in the substation first identify which feeders are affected and then trip the respective reclosers to isolate the short-circuit. Once the service restoration algorithm has isolated the fault and remapped the grid topology, the substation restores the standard operation.

### Interaction
<img src="docs/substation_bd.png"  width="672" height= "275">

### Features
<img src="docs/substation_ft_1.png"  width="511" height= "300">

## Technical Details

|                   |                               |
| ----              | ----                          |
| Dimensions        | 95 x 95 x 50 [mm] (WxLxH)     |
| Operating voltage | 5 [V]                         |
| Start delay       | 2.2 [ms]                      |

## Power consumption
The budget is based on nominal values from datasheet specifications; the typical value for led is referred to a 50% dutycycle.

### **5V bus**
| Part      | Count     | Typical | Max     |
| --------- | :-------: | ------: | ------: |
| ESP32     | 1         | 160 mA  |  260 mA |
| Fault     | 6         |  16 mA  |   96 mA |
| LED       | 12        |  60 mA  |  120 mA |
| Total     |           |         |  476 mA |

## Media

[<img src="docs/substation_1.png" width="110" height= "100">](docs/substation_1.png)
&nbsp;
[<img src="docs/substation_2.png" width="157" height= "100">](docs/substation_2.png)


## Related Links

* [Hardware]()
* [Firmware]() 
* [Assembly]()