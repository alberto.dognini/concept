# Branch

## Overview
<img src="docs/branch_fm.png"  width="421" height="300">

## Description
The branch is an active device for power distribution that allows to measure and control the power flowing among the entities. The device is based on a dual-core MCU with embedded Wi-Fi and BLE network interfaces for data logging and remote control. The MCU controls the power flow by means of two bi-directional back-to-back switches that can be used for grid reconfiguration or to emulate faults due to line interruption. A third switch, connected between the center of the bus and ground, allows the emulation of grid faults due to short-circuits to ground. The flow in the two halves of the bus is monitored by two independent monitoring units, placed respectively upstream and downstream of the fault switch in proximity of the node interconnection. These units are able to monitor the status of the grid in terms of voltage, current and power.

## Technical Details

|                   |                           |
| ----              | ----                      |
| Dimensions        | 25 x 250 x 5 [mm] (WxLxH) |
| Max bus current   | 8 [A]                     |
| Max bus voltage   | 3.6 [V]                   |
| Operating voltage | 5 [V]                     |
| Monitored bus     | 3.3 [V]                   |
| Shunt resistance  | 10 [mΩ]                   |
| Total resistance  | 40 [mΩ]                   |
| Start delay       | 0.47 [ms]                 |


## Media

[<img src="docs/branch1.png"  width="100" height="100">](docs/branch1.png)
&nbsp;
[<img src="docs/branch2.png"  width="100" height="100">](docs/branch2.png)


## Related Links

* [Hardware]()
* [Firmware]() 
* [Assembly]()

